import 'package:flutter/material.dart';

class CustomerRoute extends PageRouteBuilder{

  final Widget widget;

  CustomerRoute(this.widget)
    :super(
      transitionDuration: Duration(seconds: 1),
      pageBuilder:(
        BuildContext context,
        Animation<double> an1,
        Animation<double> an2,
      ){
        return widget;
      },
      transitionsBuilder:(
        BuildContext context,
        Animation<double> an1,
        Animation<double> an2,
        Widget child,
      ){
        /*
        //animation 1 fade
        return FadeTransition(
          opacity: Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
            parent: an1,
            curve: Curves.fastOutSlowIn
          )),
          child: child,
        );
        */

        /*
        //animation 2 zoom up/down
        return ScaleTransition(
          scale: Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
            parent: an1,
            curve: Curves.fastOutSlowIn
          )),
          child: child,
        );
        */

        /*
        //animation 3 zoom & rotation
        return RotationTransition(
          turns: Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
            parent: an1,
            curve: Curves.fastOutSlowIn
          )),
          child: ScaleTransition(
            scale: Tween(begin: 0.0, end: 1.0).animate(CurvedAnimation(
              parent: an1,
              curve: Curves.fastOutSlowIn
              )),
              child: child,
            ),
        );
        */

        //animation 4 slide
        return SlideTransition(
          position: Tween<Offset>(
            begin: Offset(-1.0, 0.0), //(x,y)
            end: Offset(0.0, 0.0),
          ).animate(CurvedAnimation(
            parent: an1,
            curve: Curves.fastOutSlowIn
          )),
          child: child,
        );
      }
    );
}